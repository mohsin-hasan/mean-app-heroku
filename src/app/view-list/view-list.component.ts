import {
	Component,
	ViewChild,
	OnChanges,
	OnInit,
	DoCheck,
	AfterContentInit,
	AfterContentChecked,
	AfterViewInit,
	AfterViewChecked,
	OnDestroy,
	TemplateRef
} from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap';
import { ToasterService } from 'angular2-toaster';

import { List } from '../models/list';
import { ListService } from '../list.service';

@Component({
	selector: 'app-view-list',
	templateUrl: './view-list.component.html',
	styleUrls: ['./view-list.component.css']
})
export class ViewListComponent implements OnChanges,
	OnInit,
	DoCheck,
	AfterContentInit,
	AfterContentChecked,
	AfterViewInit,
	AfterViewChecked,
	OnDestroy {

	@ViewChild('addListComponent') addListChildComponent: any;
	modalRef: BsModalRef;

	private lists: List[] = [];


	listItemToEditorDelete: List;

	config = {
		animated: true,
		keyboard: true,
		backdrop: true,
		ignoreBackdropClick: false
	};

	constructor(private listService: ListService,
		private modalService: BsModalService,
		private toasterService: ToasterService
	) {
		this.toasterService = toasterService;
	}

	ngOnInit() {
		this.loadLists();
		// console.log("Child-1's ngOnInit");
	}

	loadLists() {
		this.listService.getAllLists().subscribe(res => {
			this.lists = res;
		});
	}

	deleteListItem(list: List) {
		this.listService.deleteList(list._id).subscribe(res => {
			this.lists = this.lists.filter(lists => lists !== list);
			this.toasterService.pop('success', 'Success', 'Item deleted successfully !');
			this.modalRef.hide();
		});
	}

	public onAddList(newList) {
		this.lists = this.lists.concat(newList);
		this.addListChildComponent.addNewForm.form.reset();
		this.toasterService.pop('success', 'Success', 'Item added successfully !');
	}

	public onEditListItem(updatedItem) {

		this.lists.forEach((item, index) => {
			if (item._id === updatedItem._id) {
				return this.lists[index] = Object.assign({}, updatedItem);
			}
		});
		this.toasterService.pop('success', 'Success', 'Item updated successfully !');
		this.modalRef.hide();
	}

	// editRecord(list: List, index: any) {
	// 	this.listItemToEditorDelete = Object.assign({}, list);
	// }

	openModal(template: TemplateRef<any>, list, isEdit, isDelete) {
		if (isDelete) {
			this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
			this.listItemToEditorDelete = list;
		}
		// tslint:disable-next-line:one-line
		else if (isEdit) {
			this.modalRef = this.modalService.show(template, Object.assign({}, this.config, { class: 'gray modal-lg' }));
			this.listItemToEditorDelete = Object.assign({}, list);
		}
	}

	confirm() {
		if (this.listItemToEditorDelete) {
			this.deleteListItem(this.listItemToEditorDelete);
		}
	}

	decline() {
		this.modalRef.hide();
	}

	/** Methods to just to check Angular LifeCycle Hooks implementation */
	ngOnChanges() {
		// console.log("Child-1's ngOnChanges");
	}
	ngDoCheck() {
		// console.log("Child-1's ngDoCheck");
	}
	ngAfterContentInit() {
		// console.log("Child-1's ngAfterContentInit");
	}
	ngAfterContentChecked() {
		// console.log("Child-1's ngAfterContentChecked");
	}
	ngAfterViewInit() {
		// console.log("Child-1's ngAfterViewInit");
	}
	ngAfterViewChecked() {
		// console.log("Child-1's ngAfterViewChecked");
	}
	ngOnDestroy() {
		// console.log("Child-1's ngOnDestroy");
	}
}
