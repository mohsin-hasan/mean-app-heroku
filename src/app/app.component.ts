import {
	Component,
	OnChanges,
	OnInit,
	DoCheck,
	AfterContentInit,
	AfterContentChecked,
	AfterViewInit,
	AfterViewChecked,
	OnDestroy
} from '@angular/core';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent implements OnChanges,
	OnInit,
	DoCheck,
	AfterContentInit,
	AfterContentChecked,
	AfterViewInit,
	AfterViewChecked,
	OnDestroy {

	title = 'CRUD Operations With MEAN Stack';

	constructor() { }

	/** Methods to just to check Angular LifeCycle Hooks implementation */
	ngOnChanges() {
		// console.log("Parent's ngOnChanges");
	}
	ngOnInit() {
		// console.log("Parent's ngOnInit");
	}
	ngDoCheck() {
		// console.log("Parent's ngDoCheck");
	}
	ngAfterContentInit() {
		// console.log("Parent's ngAfterContentInit");
	}
	ngAfterContentChecked() {
		// console.log("Parent's ngAfterContentChecked");
	}
	ngAfterViewInit() {
		// console.log("Parent's ngAfterViewInit");
	}
	ngAfterViewChecked() {
		// console.log("Parent's ngAfterViewChecked");
	}
	ngOnDestroy() {
		// console.log("Parent's ngOnDestroy");
	}
}
