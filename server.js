const express = require('express');
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');

const bucketlist = require('./server/routes/api');
const config = require('./server/config');

const url = 'mongodb://localhost:27017/bucketlist';
// mongoose.connect(config.database);
// mongoose.connect('mongodb://localhost:27017/bucketlist');
if (process.env.MONGODB_URI) {
	mongoose.connect(process.env.MONGODB_URI);

} else {

	mongoose.connect(url, function (err) { //db = 'mongodb://localhost/yourdb'
		if (err) {
			console.log(err);
		} else {
			console.log('mongoose connection is successful on: ' + url);
		}
	});
}

const app = express();

//Middleware for CORS
app.use(cors());

//Middleware for bodyparsing using both json and urlencoding
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// // Parsers for POST data
// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: false }));

// Point static path to dist
// app.use(express.static(path.join(__dirname, 'dist/')));
var distDir = __dirname + "/dist/";
app.use(express.static(distDir));

// Set our api routes
// app.use('/api', api);
app.use('/bucketlist', bucketlist);

app.get('*', (req, res) => {
	res.send('Invalid page');
});

/**
 * Get port from environment and store in Express.
 */
const port = process.env.PORT || '3000';
app.set('port', port);

/**
 * Create HTTP server.
 */
const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port, () => console.log(`API running on local:${port}`));